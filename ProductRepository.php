<?php

class ProductRepository {
	private $products;

	public function __construct() {
		// Initialize products
		$this->products = [
			['name' => 'Red Widget', 'code' => 'R01', 'price' => 32.95],
			['name' => 'Green Widget', 'code' => 'G01', 'price' => 24.95],
			['name' => 'Blue Widget', 'code' => 'B01', 'price' => 7.95],
		];
	}

	public function findProductByCode($productCode) {
		foreach ($this->products as $i => $product) {
			if($product['code'] === $productCode) return $product;
		}
		return null;
	}

	public function all() {
		return $this->products;
	}
}