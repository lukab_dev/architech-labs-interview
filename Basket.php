<?php
// Class describing Basket
interface IBasket {
	public function add($productCode);
	public function total();
}
class Basket implements IBasket {
	private $productRepository;
	private $offers;
	private $deliveryRules;
	private $basket;

	public function __construct() {
		$this->productRepository = new ProductRepository();
		$this->initializeBasket();
	}

	private function initializeBasket() {
		// Initialize delivery rules
		$this->deliveryRules = [
			['price_threshold' => 50, 'cost' => 4.95],
			['price_threshold' => 90, 'cost' => 2.95],
			['price_threshold' => INF, 'cost' => 0],
		];

		// Initialize offers
		$this->offers = [
			['product_code' => 'R01', 'quantity_required' => 1, 'price_percentage' => 0.5],
		];

		$this->basket = [];
	}

	public function add($productCode) {
		$product = $this->productRepository->findProductByCode($productCode);

		if(!$product) {
			throw new Exception("Error adding product into the basket: No such product with code $productCode!", 1);
		}

		// If the product already exists in basket, we will increment its quantity by 1
		$productExistsInBasket = false;
		foreach ($this->basket as $i => $basketItem) {
			if($basketItem['product']['code'] === $productCode) {
				$this->basket[$i]['quantity'] += 1;
				$productExistsInBasket = true;
				break;
			}
		}

		// Otherwise, we add product in the basket with quantity 1
		if(!$productExistsInBasket) {
			$this->basket[] = [
				'product' => $product,
				'quantity' => 1,
			];
		}
	}

	// The assumption is that deliveryRules are sorted by price_threshold
	public function getDeliveryPrice($productsPrice) {
		$price = 0;
		for ($i = count($this->deliveryRules) - 1; $i >= 0; $i--) {
			$rule = $this->deliveryRules[$i];
			if($productsPrice < $rule['price_threshold']) {
				$price = $rule['cost'];
			}
			else break;
		}
		return $price;
	}

	private function calculateBasketItemPrice($basketItem) {
		$currentProduct = $basketItem['product'];
		$currentProductQuantity = $basketItem['quantity'];
		$price = 0;

		$hasSpecialOffer = false;

		// First, we check if there is special offer for this item
		foreach ($this->offers as $j => $offer) {
			// If we find one, for each group of $offer['quantity_required'] items,
			// we take one more with reduced price.
			// We first calculate the total price of one such group, multiply it with number of groups and add the remaining items with original price.
			if($offer['product_code'] === $currentProduct['code']) {
				$hasSpecialOffer = true;
				if($offer['quantity_required'] >= $currentProductQuantity) {
					$price += $currentProductQuantity * $currentProduct['price'];
				}
				else {
					$groupsCount = intval($currentProductQuantity / ($offer['quantity_required'] + 1));
					$groupPrice = $offer['quantity_required'] * $currentProduct['price'] + $currentProduct['price'] * $offer['price_percentage'];
					$price += $groupsCount * $groupPrice;

					$remainingItems = $currentProductQuantity - ($groupsCount * ($offer['quantity_required'] + 1));
					$price += $remainingItems * $currentProduct['price'];
				}
				break;
			}
		}

		// If there is no special offer, we simply multiply quantity by product price
		if(!$hasSpecialOffer) {
			$price = $currentProduct['price'] * $currentProductQuantity;
		}

		return $price;
	}

	// Assumption - there is no more than 1 offer for each product
	public function total() {
		$productsPrice = 0;
		foreach ($this->basket as $i => $basketItem) {
			$currentProductPrice = $this->calculateBasketItemPrice($basketItem);
			$productsPrice += $currentProductPrice;
		}

		return ($productsPrice + $this->getDeliveryPrice($productsPrice));
	}

	public function print() {
		foreach ($this->basket as $i => $basketItem) {
			for($j = 0; $j < $basketItem['quantity']; $j++)
				echo $basketItem['product']['code'] . ", ";
		}
	}
}