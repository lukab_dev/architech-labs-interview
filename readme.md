## Overview

- Implemented what was asked in the task. However, the code could be further refactored, but it would take too much time.  
- Defined products, product repository (catalogue), delivery rules and offers.  
- All of the above are dynamic, meaning we could simply swap delivery rules and offers for different ones and not change the rest of the code.  
- Delivery rules, offers and products were represented by associative arrays, even though it would be better to represent them by separate classes.  

# Structure of products
- name  
- code  
- price  
This is pretty self-explanatory.

# Structure of offers
- product_code  
- quantity_required  
- price_percentage  
The offer is for the particular product - quantity_required field specifies how many products of this kind should be bought in order to get discount on one more product. The price of that one is multiplied by price_percentage.  

# Structure of delivery rules
- price_threshold  
- cost  
Price threshold specifies that if total sum of products is less than that price and greater than all smaller thresholds, then the delivery cost is specified by cost field.  

# Assumptions
- Delivery rules are sorted by price_threshold  
- There is at most one offer for each product  
- The offers are of kind - if someone buys quantity_required items of product product_code, the next one has price multiplied by price_percentage.  

# Notice
- Basket items have two fields: product and quantity. Product represents whole product as associative array (or an object of a class if implemented like that). Quantity specifies how many of these products are in our basket. This means that no product is inserted twice in the basket, but rather its quantity is increased when added multiple times.